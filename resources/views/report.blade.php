<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Anvil Reporting</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="flex-center position-ref full-height" style="padding: 30px;">
    <a href="/send" class="btn btn-primary">Email Report</a><br><br>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Date</th>
            <th scope="col">Customer</th>
            @foreach ($blendTypes as $name => $data)
                <th scope="col">{{ ucfirst($name) }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach ($BYOBOrders as $order)
            <tr>
                <th scope="row">{{ $order['order_num'] }}</th>
                <td>{{ $order['order_date'] }}</td>
                <td>{{ $order['customer_name'] }}</td>
                @foreach ($blendTypes as $name => $data)
                    @if (@$order['blends'][$name])
                        <td>{{ $order['blends'][$name] }}</td>
                    @else
                        <td>0</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        <tr>
            <th scope="row" colspan="3" style="text-align:right;">Total Lbs</th>
            @foreach ($blendTypes as $name => $data)
                <td>{{ $data['count'] / 10 }}</td>
            @endforeach
        </tr>
        <tr>
            <th scope="row" colspan="3" style="text-align:right;">SKU</th>
            @foreach ($blendTypes as $name => $data)
                <td>
                    @switch($name)
                        @case('Colombia')
                            COLTenth
                        @break
                        @case('Costa_Rica')
                            CRCTenth
                        @break
                        @case('Guatemala')
                            GTMTenth
                        @break
                        @case('Ethiopia')
                            ETPTenth
                        @break
                        @case('Brazil')
                            BRZTenth
                        @break
                    @endswitch
                </td>
            @endforeach
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
