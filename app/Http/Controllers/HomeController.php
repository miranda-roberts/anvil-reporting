<?php

	namespace App\Http\Controllers;

	use Carbon\Carbon;
	use Illuminate\Support\Facades\Mail;

	class HomeController extends Controller
	{
		/**
		 * @var string
		 */
		protected $shopifyKey;

		/**
		 * @var string
		 */
		protected $shopifyPass;

		/**
		 * @var string
		 */
		protected $shopifyURL;

		/**
		 * The product id of the BYOB product
		 * @var int
		 */
		protected $productId;

		/**
		 * @var string
		 */
		protected $email;

		/**
		 * HomeController constructor.
		 */
		public function __construct()
		{
			$this->shopifyKey  = '29ac91ebabce63335cecaeb3de5cb93a';
			$this->shopifyPass = '39c24137a1b5b624afba3fa9b3b9c962';
			$this->shopifyURL  = 'anvilcoffee.myshopify.com/admin/api/2019-04/';
			$this->productId   = 1484833062966;
			$this->email       = 'miranda.roberts@ziplineinteractive.com';
		}

		/**
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function getBYOBData()
		{
			$orders = $this->makeRequest('orders.json', 'GET');
			$BYOBOrders = array();
			$blendTypes = array();

			foreach ($orders as $order) {
				$order = @$order[0];
				foreach ($order->line_items as $line_item) {
					if ($line_item->product_id == $this->productId) {
						$blendString = $line_item->properties[0]->value;
						$blendArray = explode(', ', $blendString);
						$blends = array();

						foreach ($blendArray as $blend) {
							$blendData = explode('x ', $blend);
							$blends[$blendData[1]] = $blendData[0];

							if (!@$blendTypes[$blendData[1]]) {
								$blendTypes[$blendData[1]] = [
									'count' => $blendData[0]
								];
							} else {
								$blendTypes[$blendData[1]]['count'] = $blendTypes[$blendData[1]]['count'] + $blendData[0];
							}
						}

						if ($blends) {
							$BYOBOrders[] = [
								'order_num'     => $order->order_number,
								'order_date'    => Carbon::createFromTimeString($order->created_at)->format('m/d/Y'),
								'customer_name' => $order->customer->first_name . ' ' . $order->customer->last_name,
								'blends'        => $blends
							];
						}
					}
				}
			}

			return view('report', compact('BYOBOrders', 'blendTypes'));
		}

		/**
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function sendBYOBData()
		{
			$orders = $this->makeRequest('orders.json', 'GET');
			$BYOBOrders = array();
			$blendTypes = array();

			foreach ($orders as $order) {
				$order = @$order[0];
				foreach ($order->line_items as $line_item) {
					if ($line_item->product_id == $this->productId) {
						$blendString = $line_item->properties[0]->value;
						$blendArray = explode(', ', $blendString);
						$blends = array();

						foreach ($blendArray as $blend) {
							$blendData = explode('x ', $blend);
							$blends[$blendData[1]] = $blendData[0];

							if (!@$blendTypes[$blendData[1]]) {
								$blendTypes[$blendData[1]] = [
									'count' => $blendData[0]
								];
							} else {
								$blendTypes[$blendData[1]]['count'] = $blendTypes[$blendData[1]]['count'] + $blendData[0];
							}
						}

						if ($blends) {
							$BYOBOrders[] = [
								'order_num'     => $order->order_number,
								'order_date'    => Carbon::createFromTimeString($order->created_at)->format('m/d/Y'),
								'customer_name' => $order->customer->first_name . ' ' . $order->customer->last_name,
								'blends'        => $blends
							];
						}
					}
				}
			}

			// Build a CSV to attach to email
			$filename = public_path() . "/reports/anvilreport_".date("Y-m-d_H-i",time()).".csv";
			$fp = fopen($filename, "w");

			$headerArray = array('#', 'Date', 'Customer');
			foreach ($blendTypes as $name => $data) {
				$headerArray[] = $name;
			}

			fputcsv($fp, $headerArray);

			foreach ($BYOBOrders as $order) {
				$outputArray = [$order['order_num'], $order['order_date'], $order['customer_name']];
				foreach ($blendTypes as $name => $data){
					if (@$order['blends'][ $name ]){
						$outputArray[] = $order['blends'][ $name ];
					}
				}

				fputcsv($fp, $outputArray);
			}

			$lbsArray = ['', '', 'Total Lbs'];
			foreach ($blendTypes as $name => $data) {
				$lbsArray[] = $data['count'] / 10;
			}

			fputcsv($fp, $lbsArray);

			$skuArray = ['', '', 'SKU'];
			foreach ($blendTypes as $name => $data) {
				switch($name) {
					case 'Colombia':
						$skuArray[] = 'COLTenth';
						break;
					case 'Costa_Rica':
						$skuArray[] = 'CRCTenth';
	                     break;
					case 'Guatemala':
						$skuArray[] = 'GTMTenth';
	                    break;
					case 'Ethiopia':
						$skuArray[] = 'ETPTenth';
	                     break;
					case 'Brazil':
						$skuArray[] = 'BRZTenth';
						break;
				}
			}

			fputcsv($fp, $skuArray);
			fclose($fp);

			// Send the email
			Mail::raw('Here is your report generated from the Anvil BYOB Reporter.', function($message) use ($filename) {
				$message->subject('BYOB Report')->to($this->email)->attach($filename);
			});

			echo 'Your report has been sent!';
			return view('report', compact('BYOBOrders', 'blendTypes'));
		}

		/**
		 * Make an API request to shopify
		 *
		 * @param        $url
		 * @param array  $params
		 * @param string $method
		 *
		 * @return mixed
		 */
		protected function makeRequest($url, $method = 'POST', $params = [])
		{
			$curl = curl_init();

			$curl_url = 'https://'.$this->shopifyKey.':'.$this->shopifyPass.'@'.$this->shopifyURL.$url;

			curl_setopt_array($curl, array(
				CURLOPT_URL            => $curl_url,
				CURLOPT_HEADER         => 0,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_HTTPHEADER     => array('Content-Type:application/json'),
				CURLOPT_CUSTOMREQUEST  => $method,
				CURLOPT_POSTFIELDS     => $params
			));

			$response = curl_exec($curl);
			$err      = curl_error($curl);

			curl_close($curl);

			if ($err) {
				return $err;
			} else {
				return json_decode($response);
			}
		}
	}